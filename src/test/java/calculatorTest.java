import main.java.calculadora;
import org.junit.Test;

import static org.junit.Assert.*;

public class calculatorTest {

    @Test
    public void Testsuma() {
        calculadora calcu=new calculadora();
        int result=calcu.suma(5, 4);
        assertEquals(9, result);
    }

    @Test
    public void Testresta() {
        calculadora calcu=new calculadora();
        int result=calcu.resta(5, 4);
        assertEquals(1, result);
    }

    @Test
    public void Testmultiplicacion() {
        calculadora calcu=new calculadora();
        int result=calcu.multiplicacion(5, 4);
        assertEquals(20, result);
    }

    @Test
    public void Testdivision() {
        calculadora calcu=new calculadora();
        int result=calcu.division(9, 3);
        assertEquals(3, result);
    }
}